import React from 'react';
import { Linking } from 'react-native';
import {
  Container, Content, Text, H1, H3, Button,
} from 'native-base';
import Spacer from './UI/Spacer';

const About = () => (
  <Container>
    <Content padder>
      <Spacer size={30} />
      <H1>Ismertető</H1>
      <Spacer size={10} />
      <Text>
        A Covider alkalmazás segít nyomon követni a jelenleg kialakult koronavírus járvány által okozott megbetegedéseket, haláleseteket országonként bontva.
      </Text>
      <Spacer size={10} />
      <Text>
        Lehetőség van az összes ország adatait megtekinteni, valamint készíthetünk saját ország listát is.
        {' '}
      </Text>

      <Spacer size={30} />
      <H3>Kapcsolat</H3>
      <Spacer size={10} />
      <Text>
        Az alkalmazással kapcsolatos észrevételeket, ötleteket, új funkció ajánlásokat alább küldhetik.
        {' '}
      </Text>
      <Spacer size={30} />
      <Button
        block={false}
        bordered
        style={{justifyContent: 'center'}}
        color='#039dfc'
        onPress={() => Linking.openURL('mailto:konyatamas01@gmail.com?subject=Covider&body=')}
      >
        <Text>E-mail küldése</Text>
      </Button>
    </Content>
  </Container>
);

export default About;
