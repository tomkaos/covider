import React, { Component } from 'react';
import {
  StyleSheet, View, TouchableOpacity, Image,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  Body, Card, CardItem, Text, Icon,
} from 'native-base';
import { getCountryTranslation, getCountryFlag } from '../../lib/countries';
import { Spacer } from '../UI';
import numberWithCommas from '../../lib/number';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    borderRadius: 5,
  },
  heading: {
    borderRadius: 5,
    width: '50%',
    color: '#039dfc',
    fontWeight: '700',
    fontSize: 16,
  },
  data: {
    fontWeight: '500',
    width: '50%',
  },
  topcases: {
    fontWeight: '700',
  },
  flag: {
    width: 35,
    height: 25,
    marginRight: 10,
  },
});

// eslint-disable-next-line react/prefer-stateless-function
class SingleItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      opened: props.opened,
    };
  }

  toggle = () => {
    // eslint-disable-next-line react/destructuring-assignment
    if (!this.props.alwaysOpen) {
      this.setState((prevState) => ({
        opened: !prevState.opened,
      }));
    }
  };

  render() {
    const { item, alwaysOpen, canRemove, onRemove } = this.props;
    const { opened } = this.state;
    const { url } = getCountryFlag(item.country);
    return (
      <Card style={{ borderRadius: 5 }}>
        <TouchableOpacity onPress={this.toggle}>
          {/* eslint-disable-next-line max-len */}
          <CardItem
            header
            bordered
            style={
              (alwaysOpen || opened) ? {
                justifyContent: 'space-between',
                backgroundColor: '#039dfc',
                borderRadius: 5,
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0,
              } : {
                borderRadius: 5,
                justifyContent: 'space-between',
              }
            }
          >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              {/* eslint-disable-next-line import/no-dynamic-require */}
              {url && <Image style={styles.flag} source={url} />}
              <Text style={[{ fontWeight: '700', fontSize: 20 }, (alwaysOpen || opened) ? { color: 'white' } : null]}>
                {getCountryTranslation(item.country)}
              </Text>
            </View>
            <Text style={{ ...styles.topcases, ...{ color: (alwaysOpen || opened) ? 'white' : '#039dfc' } }}>
              {numberWithCommas(item.cases)}
            </Text>
          </CardItem>
        </TouchableOpacity>
        {(alwaysOpen || opened) && (
          <CardItem cardBody>
            <Body style={{ paddingHorizontal: 15 }}>
              <Spacer size={10} />
              <View style={styles.row}>
                <Text style={styles.heading}>
                  Összes eset
                </Text>
                <Text style={styles.heading}>
                  Mai új esetek
                </Text>
              </View>
              <View style={{
                ...styles.row,
                marginBottom: 10,
              }}
              >
                <Text style={styles.data}>
                  {numberWithCommas(item.cases)}
                </Text>
                <Text style={{ ...styles.data, color: item.todayCases > 0 ? 'orange' : 'black' }}>
                  {numberWithCommas(item.todayCases)}
                </Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.heading}>
                  Összes elhunyt
                </Text>
                <Text style={styles.heading}>
                  Mai elhunytak
                </Text>
              </View>
              <View style={{
                ...styles.row,
                marginBottom: 10,
              }}
              >
                <Text style={styles.data}>
                  {numberWithCommas(item.deaths)}
                </Text>
                <Text style={{ ...styles.data, color: item.todayDeaths > 0 ? 'red' : 'black' }}>
                  {numberWithCommas(item.todayDeaths)}
                </Text>
              </View>
              <Spacer size={5} />
              <View style={styles.row}>
                <Text style={styles.heading}>Gyógyultak</Text>
                <Text style={styles.heading}>Kezelés alatt</Text>
              </View>
              <View style={{
                ...styles.row,
                marginBottom: 10,
              }}
              >
                <Text style={{ ...styles.data, color: 'green' }}>
                  {numberWithCommas(item.recovered)}
                </Text>
                <Text style={{ ...styles.data }}>
                  {numberWithCommas(item.active)}
                </Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.heading}>
                  Kritikus állapotban
                </Text>
              </View>
              <View style={{
                ...styles.row,
                marginBottom: 10,
              }}
              >
                <Text style={{ ...styles.data, color: 'red' }}>
                  {numberWithCommas(item.critical)}
                </Text>
                {canRemove && (
                  <TouchableOpacity
                    onPress={() => {
                      this.toggle();
                      onRemove();
                    }}
                    style={{
                      borderRadius: 5,
                      padding: 5,
                      paddingHorizontal: 15,
                      backgroundColor: 'red',
                    }}
                  >
                    <Icon name="trash" style={{ color: 'white' }} />
                  </TouchableOpacity>
                )}
              </View>
              <Spacer size={10} />
            </Body>
          </CardItem>
        )}
      </Card>
    );
  }
}

SingleItem.propTypes = {
  item: PropTypes.objectOf(PropTypes.any).isRequired,
  opened: PropTypes.bool,
  alwaysOpen: PropTypes.bool,
  canRemove: PropTypes.bool,
  onRemove: PropTypes.func,
};

SingleItem.defaultProps = {
  opened: false,
  alwaysOpen: false,
  canRemove: false,
  onRemove: null,
};

export default SingleItem;
