import Colors from '../../native-base-theme/variables/commonColor';
import { Platform } from 'react-native';

export default {
  navbarProps: {
    navigationBarStyle: { backgroundColor: Platform.OS === 'ios' ? 'white' : '#039dfc' },
    titleStyle: {
      color: Platform.OS === 'ios' ? Colors.textColor : 'white',
      alignSelf: 'center',
      fontSize: Colors.fontSizeBase,
    },
    backButtonTintColor: Colors.textColor,
  },

  tabProps: {
    swipeEnabled: false,
    activeBackgroundColor: Colors.brandLight,
    inactiveBackgroundColor: 'white',
    tabBarStyle: { backgroundColor: 'white' },
  },

  icons: {
    style: { color: Colors.brandPrimary, height: 30, width: 30 },
  },
};

// Colors.brandPrimary
