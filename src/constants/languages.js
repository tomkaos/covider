const languages = [
  {
    code: 'HU',
    title: 'Magyar',
  },
  {
    code: 'EN',
    title: 'English',
  },
];

// eslint-disable-next-line import/prefer-default-export
export { languages };
