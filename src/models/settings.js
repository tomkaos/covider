import initialState from '../store/settings';

export default {
  namespace: 'settings',

  /**
   *  Initial state
   */
  state: initialState,

  /**
   * Effects/Actions
   */
  effects: (dispatch) => ({
    updateCountries(updatedCountries, rootState) {
      const { ownCountries } = rootState;
      if (ownCountries !== updatedCountries) {
        return dispatch.settings.replaceCountries(updatedCountries);
      }
      return true;
    },
  }),

  /**
   * Reducers
   */
  reducers: {
    replaceCountries(state, payload) {
      return {
        ...state,
        ownCountries: payload,
      };
    },
  },
};
