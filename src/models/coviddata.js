import moment from 'moment';
import Api from '../lib/api';
import HandleErrorMessage from '../lib/format-error-messages';
import initialState from '../store/coviddata';

export default {
  namespace: 'coviddata',

  /**
   *  Initial state
   */
  state: initialState,

  /**
   * Effects/Actions
   */
  effects: (dispatch) => ({

    async fetchList({ forceSync = false } = {}, rootState) {
      const { coviddata = {} } = rootState;
      const { lastSync = null } = coviddata;
      // Only sync when it's been 5mins since last sync
      // console.log(lastSync);
      if (lastSync) {
        if (!forceSync && moment().isBefore(moment(lastSync).add(5, 'minutes'))) {
          return true;
        }
      }

      try {
        const responseGlobal = await Api.get('/all');
        // console.log(responseGlobal);
        const globalData = responseGlobal.data;
        const response = await Api.get('/countries');
        // console.log(response);
        const { data } = response;
        if (!data || data.length < 1) {
          return true;
        }
        return dispatch.coviddata.replace({ data, globalData });
      } catch (error) {
        throw HandleErrorMessage(error);
      }
    },
  }),

  /**
   * Reducers
   */
  reducers: {
    /**
     * Replace list in store
     * @param {obj} state
     * @param {obj} payload
     */
    replace(state, payload) {
      let newList = null;
      let newGlobalList = null;
      const { data, globalData } = payload;

      // Loop data array, saving items in a usable format
      if (data && typeof data === 'object' && globalData && typeof globalData === 'object') {
        newList = data;
        // eslint-disable-next-line array-callback-return
        /*newList.map((x) => {
          console.log(x.country);
        });*/
        newGlobalList = globalData;
      }
      return newList && newGlobalList
        ? {
          ...state,
          countries: newList,
          global: newGlobalList,
          lastSync: moment().format(),
        }
        : initialState;
    },
  },
};
